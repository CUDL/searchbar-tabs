# iDiscover tabbed interface source files

Source files for the [`primo-explore-cam-search-bar-tabs`](https://bitbucket.org/CUDL/primo-explore-cam-search-bar-tabs) 
package. The steps outlined in this repo should be followed if the tabbed interface 
does not work with a new Primo release.


## Installing and generating tabbed interface js and css files

1. Clone the repo in `<primo dev env root>/primo-explore/custom`.

2. Extract `searchBar.html` from the Primo bundle of the latest release.

3. Save new `searchBar.html` in `<repo root>/html/searchBar.html`.

4. Add directives to appropriate locations in  `searchBar.html`. [Example file available here](https://bitbucket.org/snippets/CUDL/9eBLeE) and the directives are:
    * `<cam-tabs selected-scope="$ctrl.scopeField" selected-tab="$ctrl.selectedTab"></cam-tabs>`
    * `<cam-browse-button class="switch-to-advanced zero-margin button-with-icon"></cam-browse-button>`

5. Run `gulp run --view searchbar-tabs --proxy http://idiscover.lib.cam.ac.uk --browserify`

6. Verify at `http://localhost:8003/primo-explore/search?vid=44CAM_PROD&lang=en_US` that all tests pass.

7. Copy the generated `js/custom.js` as `js/primo-explore-cam-search-bar-tabs.js` and update [`primo-explore-cam-search-bar-tabs`](https://bitbucket.org/CUDL/primo-explore-cam-search-bar-tabs)

8. Do the same for `js/custom1.css` leaving the filename as-is.


## Extract `searchBar.html`

Go to the [Sharepoint site for Digital Services](https://universityofcambridgecloud.sharepoint.com/:f:/s/CUL/departments/DIS/digitalservices/EjGhlxpnc1tGgKhtiWUx_6QBr_LOsx_51QAxczkCxuPxxQ?e=PAtu5Q) 
and detailed notes with additional files are available.
